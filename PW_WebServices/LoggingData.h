#ifndef LOGGINGDATA_H
#define LOGGINGDATA_H
#include "pw_webservices_lib_global.h"

#include <QString>

/**
 * @brief The LoginData class
 */
class PW_WEBSERVICES_LIBSHARED_EXPORT LoginData
{
public:
    /**
     * @brief Empty constructor
     */
    LoginData();

    /**
     * @brief get User Name
     * @return User Name
     */
    QString getUserName() const;
    /**
     * @brief set User Name
     * @param User Name
     */
    void setUserName(const QString &value);
    /**
     * @brief get Password
     * @return Password
     */
    QString getPassword() const;
    /**
     * @brief set Password
     * @param Password
     */
    void setPassword(const QString &value);
    /**
     * @brief get Token
     * @return Token
     */
    QString getToken() const;
    /**
     * @brief set Token
     * @param Token
     */
    void setToken(const QString &value);
    /**
     * @brief get Nonce
     * @return Nonce
     */
    QString getNonce() const;
    /**
     * @brief set Nonce
     * @param  Nonce
     */
    void setNonce(const QString &value);
    /**
     * @brief isLogged
     * @return true if use is logged
     */
    bool isLogged() const;
    /**
     * @brief set Logged
     * @param login status
     */
    void setLogged(bool value);
    /**
     * @brief get User Id
     * @return User unique Id
     */
    QString getUserId() const;
    /**
     * @brief set User Id
     * @param User unique id
     */
    void setUserId(const QString &value);
    /**
     * @brief logout
     */
    void logout();

private:
    /**
     * @brief  User Name
     */
    QString mUserName;
    /**
     * @brief Password
     */
    QString mPassword;
    /**
     * @brief Token
     */
    QString mToken;
    /**
     * @brief Nonce
     */
    QString mNonce;
    /**
     * @brief UserId
     */
    QString mUserId;
    /**
     * @brief Logged
     */
    bool mLogged;
};

#endif // LOGGINGDATA_H
