#ifndef PROJECT_H
#define PROJECT_H
#include "pw_webservices_lib_global.h"

#include <QList>
#include <QStringList>
#include <QMap>
#include <QVector>

#include "PWImage.h"
#include "PW3dPoint.h"
#include "PW2dPoint.h"
#include "PWGraphImages.h"

/**
 * @brief The Project class
 */
class PW_WEBSERVICES_LIBSHARED_EXPORT Project
{
public:
    /**
     * @brief Empty constructor
     */
    Project();

    ~Project();
    /**
     * @brief get unique ID
     * @return unique ID
     */
    int     getID();
    /**
     * @brief get Name
     * @return Name
     */
    QString getName();
    /**
     * @brief get Base Path
     * @return Base Path
     */
    QString getBasePath();
    /**
     * @brief get PreProcessing Path
     * @return PreProcessing Path
     */
    QString getPreProcessingPath();
    /**
     * @brief get Sparse Model Full Path
     * @return Sparse Model Full Path
     */
    QString getSparseModelFullPath();
    /**
     * @brief get Dense Model Full Path
     * @return Dense Model Full Path
     */
    QString getDenseModelFullPath();
    /**
     * @brief get Sparse Model Relative Path
     * @return Sparse Model Relative Path
     */
    QString getSparseModelRelativePath();
    /**
     * @brief get Dense Model Relative Path
     * @return Dense Model Relative Path
     */
    QString getDenseModelRelativePath();
    /**
     * @brief get Description
     * @return Description
     */
    QString getDescription();
    /**
     * @brief get Images List
     * @return Images List
     */
    QList<PWImage*> getImages();
    /**
     * @brief get Images File Name List
     * @return Images File Name List
     */
    QVector<QString> getImagesFileName();
    /**
     * @brief get Image By URL
     * @param image url
     * @return Image
     */
    PWImage *getImageByURL(QString url);
    /**
     * @brief get Image By Name
     * @param Image name
     * @return Image
     */
    PWImage *getImageByName(QString name);
    /**
     * @brief get Cloud UUID
     * @return Cloud UUID
     */
    QString getCloudUUID();
    /**
     * @brief set unique ID
     * @param unique id
     */
    void setID(int id);
    /**
     * @brief set Name
     * @param name
     */
    void setName(QString name);
    /**
     * @brief set Base Path
     * @param base path
     */
    void setBasePath(QString path);
    /**
     * @brief set Preprocessing Path
     * @param Preprocessing path
     */
    void setPreprocessingPath(QString path);

    /**
     * @brief Sets the Sparse model path, relative to basePath
     * @param Sparse model path
     */
    void setSparseModelRelativePath(QString path);
    /**
     * @brief Sets the Dense model path, relative to basePath
     * @param Dense model path
     */
    void setDenseModelRelativePath(QString path);
    /**
     * @brief set Description
     * @param description
     */
    void setDescription(QString description);
    /**
     * @brief add Imput Images
     * @param input images List
     */
    void addImputImages (QList<PWImage*> imagesList);
    /**
     * @brief gets saved status
     * @return saved satus (false=already saved)
     */
    bool needToSave();
    /**
     * @brief set saved status
     * @param saved status
     */
    void setNeedToSave(bool needToSave);
    /**
     * @brief set Cloud UUID
     * @param Cloud UUID
     */
    void setCloudUUID(QString value);

    /**
     * @brief add Imput Images
     * @param images Path List
     * @return number of added images
     */
    int addImputImages (QStringList imagePathList);  
    /**
     * @brief remove Image By Name
     * @param Image name
     */
    void removeImageByName(QString name);
    /**
     * @brief remove Images By Name
     * @param image names list
     */
    void removeImagesByName(QStringList namesList);

private:
    /**
     * @brief Persistence unique id.
     */
    int mID;
    /**
     * @brief Name
     */
    QString mName;
    /**
     * @brief Description
     */
    QString mDescription;
    /**
     * @brief Input Images
     */
    QList<PWImage *> mInputImages;
    /**
     * @brief Base Path
     */
    QString mBasePath;
    /**
     * @brief Preprocessing Path
     */
    QString mPreprocessingPath;
    /**
     * @brief Sparse Model Path
     */
    QString mSparseModelPath;
    /**
     * @brief Dense Model Path
     */
    QString mDenseModelPath;
    /**
     * @brief Need To Save
     */
    bool mNeedToSave;
    /**
     * @brief Cloud UUID
     */
    QString mCloudUUID;
};


#endif // PROJECT_H
