#ifndef PWIMAGE_H
#define PWIMAGE_H
#include "pw_webservices_lib_global.h"

#include <QString>

#include "PW2dPoint.h"


/**
 * @brief The PWImage class Symple representation of an image.
 */
class  PW_WEBSERVICES_LIBSHARED_EXPORT PWImage
{
public:
    /**
     * @brief empty constructor
     */
    PWImage();
    ~PWImage();
    /**
     * @brief constructor with url
     * @param url
     */
    PWImage(QString url);
    /**
     * @brief get unique ID
     * @return unique ID
     */
    int getID();
    /**
     * @brief get Full Path
     * @return Full Path
     */
    QString getFullPath();
    /**
     * @brief get File Name
     * @return File Name
     */
    QString getFileName();
    /**
     * @brief get Control Points
     * @return Control Points
     */
    QList<PW2dPoint *> *getControlPoints();
    /**
     * @brief get Size
     * @return Size
     */
    QSize getSize();
    /**
     * @brief set unique ID
     * @param unique id
     */
    void setID(int id);
    /**
     * @brief set Base Path
     * @param url
     */
    void setBasePath(QString url);
    /**
     * @brief set Full Path
     * @param basePath
     */
    void setFullPath(QString basePath){mBasePath=basePath;};


private:
    /**
     * @brief mID Persistence unique id.
     */
    int mID;
    /**
     * @brief  Directory containing image
     */
    QString mBasePath;
    /**
     * @brief  File name (relative to mUrl).
     */
    QString mFileName;
    /**
     * @brief Control Points
     */
    QList<PW2dPoint *> mControlPoints;
    /**
     * @brief Size
     */
    QSize mSize;
};

#endif // PWIMAGE_H
