#ifndef SHA256_H
#define SHA256_H
#include "pw_webservices_lib_global.h"

#include <string>
 
/**
 * @brief The SHA256 class
 */
class SHA256
{
protected:
    /**
     * @brief uint8
     */
    typedef unsigned char uint8;
    /**
     * @brief uint32
     */
    typedef unsigned long uint32;
    /**
     * @brief uint64
     */
    typedef unsigned long long uint64;
    /**
     * @brief sha256_k
     */
    const static uint32 sha256_k[];
    /**
     * @brief SHA224_256_BLOCK_SIZE
     */
    static const unsigned int SHA224_256_BLOCK_SIZE = (512/8);
public:
    /**
     * @brief init
     */
    void init();
    /**
     * @brief update
     * @param message
     * @param len
     */
    void update(const unsigned char *message, unsigned int len);
    /**
     * @brief final
     * @param digest
     */
    void final(unsigned char *digest);
    /**
     * @brief DIGEST_SIZE
     */
    static const unsigned int DIGEST_SIZE = ( 256 / 8);
 
protected:
    /**
     * @brief transform
     * @param message
     * @param block_nb
     */
    void transform(const unsigned char *message, unsigned int block_nb);
    /**
     * @brief m_tot_len
     */
    unsigned int m_tot_len;
    /**
     * @brief m_len
     */
    unsigned int m_len;
    /**
     * @brief m_block
     */
    unsigned char m_block[2*SHA224_256_BLOCK_SIZE];
    /**
     * @brief m_h
     */
    uint32 m_h[8];
};
 
std::string sha256(std::string input);
 
#define SHA2_SHFR(x, n)    (x >> n)
#define SHA2_ROTR(x, n)   ((x >> n) | (x << ((sizeof(x) << 3) - n)))
#define SHA2_ROTL(x, n)   ((x << n) | (x >> ((sizeof(x) << 3) - n)))
#define SHA2_CH(x, y, z)  ((x & y) ^ (~x & z))
#define SHA2_MAJ(x, y, z) ((x & y) ^ (x & z) ^ (y & z))
#define SHA256_F1(x) (SHA2_ROTR(x,  2) ^ SHA2_ROTR(x, 13) ^ SHA2_ROTR(x, 22))
#define SHA256_F2(x) (SHA2_ROTR(x,  6) ^ SHA2_ROTR(x, 11) ^ SHA2_ROTR(x, 25))
#define SHA256_F3(x) (SHA2_ROTR(x,  7) ^ SHA2_ROTR(x, 18) ^ SHA2_SHFR(x,  3))
#define SHA256_F4(x) (SHA2_ROTR(x, 17) ^ SHA2_ROTR(x, 19) ^ SHA2_SHFR(x, 10))
#define SHA2_UNPACK32(x, str)                 \
{                                             \
    *((str) + 3) = (uint8) ((x)      );       \
    *((str) + 2) = (uint8) ((x) >>  8);       \
    *((str) + 1) = (uint8) ((x) >> 16);       \
    *((str) + 0) = (uint8) ((x) >> 24);       \
}
#define SHA2_PACK32(str, x)                   \
{                                             \
    *(x) =   ((uint32) *((str) + 3)      )    \
           | ((uint32) *((str) + 2) <<  8)    \
           | ((uint32) *((str) + 1) << 16)    \
           | ((uint32) *((str) + 0) << 24);   \
}
#endif
