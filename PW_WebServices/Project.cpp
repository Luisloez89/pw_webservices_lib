#include <math.h>
#include <QDebug>
#include <QFile>
#include <QDir>

#include "Project.h"

Project::Project():
    mName(""),
    mNeedToSave(false),
    mCloudUUID(""),
    mID(0),
    mPreprocessingPath("")
{
}

Project::~Project() {
    for(int i = 0; i< mInputImages.count(); i++)
        delete mInputImages.at(i);
}

int Project::getID()
{
    return mID;
}

void Project::addImputImages(QList<PWImage*> imagesList)
{
    mInputImages.append(imagesList);
    mNeedToSave = true;
}

bool Project::needToSave()
{
    return mNeedToSave;
}

void Project::setNeedToSave(bool needToSave)
{
    mNeedToSave = needToSave;
}

int Project::addImputImages(QStringList imagePathList)
{
    for (int i=0; i<imagePathList.count(); i++){
        mInputImages.append(new PWImage(imagePathList.at(i)));
    }
    mNeedToSave = true;
    return 0;
}

QList<PWImage *> Project::getImages()
{
    return mInputImages;
}

QVector<QString> Project::getImagesFileName()
{
    QVector<QString> imagesFileName;
    for(int i=0;i<mInputImages.size();i++)
    {
        imagesFileName.push_back(mInputImages.at(i)->getFileName());
    }
    return(imagesFileName);
}

PWImage *Project::getImageByURL(QString url)
{
    for(int i=0; i<mInputImages.count();i++){
        if (mInputImages.at(i)->getFullPath() == url)
            return mInputImages.at(i);
    }
    return NULL;
}

PWImage *Project::getImageByName(QString name)
{
    for(int i=0; i<mInputImages.count();i++){
        if (mInputImages.at(i)->getFileName() == name)
            return mInputImages.at(i);
    }
    return NULL;
}

void Project::removeImageByName(QString name)
{
    QMutableListIterator<PWImage*> i(mInputImages);
    while (i.hasNext()) {
        if (name == i.next()->getFileName()){
            i.remove();
        }
    }
    mNeedToSave = true;
}

void Project::removeImagesByName(QStringList namesList)
{
    QMutableListIterator<PWImage*> i(mInputImages);
    while (i.hasNext()) {
        QString name = i.next()->getFileName();
        if (namesList.contains(name)){
            i.remove();
        }
    }
    mNeedToSave = true;
}

void Project::setID(int id)
{
    mID = id;
}

void Project::setName(QString name)
{
    mName = name;
}

void Project::setBasePath(QString path)
{
    mBasePath = path;
    for (int i=0; i<mInputImages.count(); i++)
        mInputImages.at(i)->setBasePath(path);
}

void Project::setPreprocessingPath(QString path)
{
    mPreprocessingPath = path;
}

QString Project::getName()
{
    return mName;
}

QString Project::getBasePath()
{
    return mBasePath;
}

QString Project::getPreProcessingPath()
{
    if (mPreprocessingPath.isEmpty())
        return mBasePath;
    return mPreprocessingPath;
}

QString Project::getCloudUUID()
{
    return mCloudUUID;
}

void Project::setCloudUUID(QString value)
{
    mCloudUUID = value;
}

QString Project::getSparseModelFullPath()
{
    return mBasePath + "/" + mSparseModelPath;
}

QString Project::getDenseModelFullPath()
{
    return mBasePath + "/" +  mDenseModelPath;
}

QString Project::getSparseModelRelativePath()
{
    return mSparseModelPath;
}

QString Project::getDenseModelRelativePath()
{
    return mDenseModelPath;
}


void Project::setSparseModelRelativePath(QString path)
{
    mSparseModelPath = path;
    mNeedToSave = true;
}

void Project::setDenseModelRelativePath(QString path)
{
    mDenseModelPath = path;
    mNeedToSave = true;
}

QString Project::getDescription()
{
    return mDescription;
}



void Project::setDescription(QString description)
{
    mDescription = description;
    mNeedToSave = true;
}

