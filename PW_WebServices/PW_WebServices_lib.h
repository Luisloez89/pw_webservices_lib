#ifndef PW_WEBSERVICES_LIB_H
#define PW_WEBSERVICES_LIB_H

#include "pw_webservices_lib_global.h"

/**
 * @brief The PW_WebServices_lib class
 */
class PW_WEBSERVICES_LIBSHARED_EXPORT PW_WebServices_lib
{

public:
    PW_WebServices_lib();
};

#endif // PW_WEBSERVICES_LIB_H
