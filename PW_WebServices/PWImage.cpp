#include <QRegExp>
#include <QImageReader>
#include <QFileInfo>

#include "PWImage.h"


PWImage::PWImage():
    mID(0)
{
}

PWImage::PWImage(QString url):
    mID(0)
{
    QFileInfo fileInfo(url);
    mFileName=fileInfo.fileName();
    mBasePath=fileInfo.absolutePath();
    QImageReader imageReader(url);
    mSize = imageReader.size();
}

PWImage::~PWImage(){

}

int PWImage::getID()
{
    return mID;
}

QString PWImage::getFullPath()
{
    return mBasePath + "/" + mFileName;
}

void PWImage::setBasePath(QString url)
{
    mBasePath = url;
}

QString PWImage::getFileName()
{
    return mFileName;
}

QList<PW2dPoint *> * PWImage::getControlPoints()
{
    return &mControlPoints;
}


QSize PWImage::getSize()
{
    return mSize;
}

void PWImage::setID(int id)
{
    mID = id;
}
