#ifndef PW2DPOINT_H
#define PW2DPOINT_H
#include "pw_webservices_lib_global.h"

#include <QPointF>

#include "PWPoint.h"

/**
 * @brief The PW2dPoint class
 */
class PW_WEBSERVICES_LIBSHARED_EXPORT PW2dPoint : public PWPoint, public QPointF
{
public:
    /**
     * @brief Constructor empty/point type
     * @param type
     */
    PW2dPoint(PWPoint::PointType type = PWPoint::Control);
    /**
     * @brief constructor with params
     * @param x
     * @param y
     * @param name
     * @param type
     */
    PW2dPoint(double x, double y, QString name = "", PWPoint::PointType type = PWPoint::Control);

    /**
     * @brief Gets de number of components or dimensions of the point
     * @return
     */
    virtual int getCoordsCount();

    /**
     * @brief getCoord Gets the value of a component or coordinate.
     * @param index
     * @return
     */
    virtual double getCoord(int index);


    /**
     * @brief getPoint Gets the 2d Point.
     * @return
     */
    virtual QPointF getPoint();

    /**
     * @brief setCoord Sets the value of a component or coordinate.
     * @param index
     * @param value
     */
    virtual void setCoord(int index, double value);


};

#endif // PW2DPOINT_H
