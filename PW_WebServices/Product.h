#ifndef PRODUCT_H
#define PRODUCT_H
#include "pw_webservices_lib_global.h"

#include <QObject>

/**
 * @brief The Product class
 */
class PW_WEBSERVICES_LIBSHARED_EXPORT Product : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     * @param QObject
     */
    explicit Product(QObject *parent = 0);

    /**
     * @brief get UUID
     * @return UUID
     */
    QString getUUID();
    /**
     * @brief set UUID
     * @param UUID
     */
    void setUUID(QString value);
    /**
     * @brief get File Name
     * @return File Name
     */
    QString getFileName();
    /**
     * @brief set File Name
     * @param File Name
     */
    void setFileName(QString value);
    /**
     * @brief get Type
     * @return Type
     */
    QString getType();
    /**
     * @brief set Type
     * @param Type
     */
    void setType( QString value);
    /**
     * @brief getProjectUUID
     * @return
     */
    QString getProjectUUID() ;
    /**
     * @brief set Project UUID
     * @param Project UUID
     */
    void setProjectUUID(QString value);

signals:

public slots:

private:
    /**
     * @brief UUID
     */
    QString mUUID;
    /**
     * @brief FileName
     */
    QString mFileName;
    /**
     * @brief Type
     */
    QString mType;
    /**
     * @brief ProjectUUID
     */
    QString mProjectUUID;

};

#endif // PRODUCT_H
