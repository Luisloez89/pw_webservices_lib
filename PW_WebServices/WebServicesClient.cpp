#include "WebServicesClient.h"

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkProxy>
#include <QScriptValueIterator>
#include <QHttpMultiPart>
#include <QScriptEngine>
#include <QDateTime>
#include "PW_WebServices/sha256.h"
#include <QFile>

WebServicesClient::WebServicesClient(QString baseURL):
    mBaseURL(baseURL),
    mUploadedImages(0),
    mProject(0),
    mUploadedProducts(0),
    mCanceled(false),
    mCurrentReply(0)
{
    mLoginData = new LoginData();
    mNetwork = new QNetworkAccessManager(this);
}

WebServicesClient::~WebServicesClient(){
    delete mLoginData;
}

QString WebServicesClient::getBaseURL()
{
    return mBaseURL;
}

void WebServicesClient::setBaseURL(QString &baseURL)
{
    mBaseURL = baseURL;
}

bool WebServicesClient::isLogged()
{
    return mLoginData->isLogged();
}

void WebServicesClient::login(QString user, QString password)
{
    QString serviceURL = mBaseURL+"/service/user/login/";
    QByteArray  params = "{\"username\":\""+user.toLatin1()+"\",\"password\":\""+password.toLatin1()+"\"}";
    QNetworkRequest request;

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setUrl(QUrl(serviceURL));
    connect(mNetwork, SIGNAL(finished(QNetworkReply*)),this, SLOT(onLoginFinished(QNetworkReply*)));
    mLoginData->setUserName(user);
    mLoginData->setPassword(password);
    QNetworkReply *reply = mNetwork->post(request, params);
}

int WebServicesClient::getSessionStatus()
{
    //TODO:
    return 0;
}

void WebServicesClient::setAuthorization(QNetworkRequest &request, QString service, int requestType)
{
    setAuthorization(request, service, requestType, mLoginData->getToken(), mLoginData->getUserId());
}

void WebServicesClient::addProject(Project *project)
{
    mCanceled=false;
    QNetworkRequest request;
    QString serviceURL = mBaseURL+"/service/project/add";
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QByteArray  params = "{\"name\":\"" + project->getName().toLatin1() +  "\",";
    params += "\"description\":\""+project->getDescription().toLatin1()+"\",";
    params += "\"sparsePath\":\""+project->getSparseModelRelativePath().toLatin1()+"\",";
    params += "\"densePath\":\""+project->getDenseModelRelativePath().toLatin1()+"\",";
    params += "\"metadata\":\"kkkkk\",";
    params += "\"user\":\""+mLoginData->getUserId().toLatin1()+"\"}";

    request.setUrl(QUrl(serviceURL));
    setAuthorization(request, "service/project/add", WEBSERVICE_REQUEST_TYPE_POST);

    QObject::connect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(onAddProjectFinished(QNetworkReply *)));
    QNetworkReply *reply = mNetwork->post(request,params);
}

void WebServicesClient::chageStatus(Project *project, QString status)
{

    QNetworkRequest request;
    QString serviceURL = mBaseURL+"/service/status/add";
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QByteArray  params = "{\"status\":\"" + status.toLatin1() +  "\",";
    params += "\"description\":\""+status+"\",";
    params += "\"project\":\""+project->getCloudUUID().toLatin1()+"\"}";


    request.setUrl(QUrl(serviceURL));
    setAuthorization(request, "service/status/add", WEBSERVICE_REQUEST_TYPE_POST);

    QObject::connect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(onChangeStatusFinished(QNetworkReply *)));
    QNetworkReply *reply = mNetwork->post(request,params);
}

void WebServicesClient::addImages(Project *project)
{
    mCanceled=false;
    QString uuid = project->getCloudUUID();

    if (uuid.isEmpty()) /// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return;

    mUploadedImages = 0;
    mProject = project;

    QNetworkRequest request;
    QString serviceURL = mBaseURL+"/service/imagen-project/add";
    request.setUrl(QUrl(serviceURL));
    //    request.setHeader(QNetworkRequest::ContentTypeHeader, "multipart/form-data");

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart textPart;
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"project\""));
    textPart.setBody(uuid.toLatin1());

    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/png"));
    QString fileName =  project->getImages().at(0)->getFileName();
    QString header = "form-data; name=\"file\"";
    header += "; filename=\"" + fileName + "\"";
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(header));
    QFile *file = new QFile(project->getImages().at(0)->getFullPath());
    file->open(QIODevice::ReadOnly);
    imagePart.setBodyDevice(file);
    file->setParent(multiPart); // we cannot delete the file now, so delete it with the multiPart

    multiPart->append(textPart);
    multiPart->append(imagePart);

    setAuthorization(request, "service/imagen-project/add", WEBSERVICE_REQUEST_TYPE_POST);
    connect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(onAddImageFinished(QNetworkReply*)));
    emit statusChanged(0, "Uploading " + project->getImages().at(0)->getFileName());

    QNetworkReply *reply = mNetwork->post(request,multiPart);
}

void WebServicesClient::addProducts(Project *project)
{
    mCanceled=false;
    QString uuid = project->getCloudUUID();

    //TODO ???
    if (uuid.isEmpty()) /// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return;

    mProject = project;
    mUploadedProducts = 0;

    QNetworkRequest request;
    QString serviceURL = mBaseURL+"/service/product/add";
    request.setUrl(QUrl(serviceURL));

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart textPartProject;
    textPartProject.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"project\""));
    textPartProject.setBody(uuid.toLatin1());

    QHttpPart textPartType;
    textPartType.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"typeFile\""));
    textPartType.setBody(QByteArray("sparse"));

    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/png"));
    QString fileName =  project->getSparseModelRelativePath();
    QString header = "form-data; name=\"file\"";
    header += "; filename=\"" + fileName + "\"";
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(header));
    QFile *file = new QFile(project->getSparseModelFullPath());
    file->open(QIODevice::ReadOnly);
    imagePart.setBodyDevice(file);
    file->setParent(multiPart); // we cannot delete the file now, so delete it with the multiPart

    multiPart->append(textPartProject);
    multiPart->append(textPartType);
    multiPart->append(imagePart);

    setAuthorization(request, "service/product/add", WEBSERVICE_REQUEST_TYPE_POST);
    QObject::connect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(onAddProductsFinished(QNetworkReply*)));
    emit statusChanged(0, "Uploading " + mProject->getSparseModelRelativePath());

    QNetworkReply *reply = mNetwork->post(request,multiPart);

}

void WebServicesClient::getProducts(QList<Product *> products)
{
    mCanceled=false;
    mProducts = products;
    mDownloadedProducts=0;
    Product *product = mProducts.at((mDownloadedProducts));

    emit statusChanged(mDownloadedProducts, "Downloading "+ product->getFileName());
    getProduct(product);
}

void WebServicesClient::getProduct(Project *project, QString type)
{

}

void WebServicesClient::getProduct(Product *product)
{
    QNetworkRequest request;
    QString serviceURL = mBaseURL+"/service/product/download/"+product->getUUID();
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    request.setUrl(QUrl(serviceURL));
    setAuthorization(request, "service/product/download/"+product->getUUID(), WEBSERVICE_REQUEST_TYPE_GET);

    QObject::connect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(onGetProductFinished(QNetworkReply*)));
    mCurrentReply = mNetwork->get(request);
}

void WebServicesClient::getProductsList(Project *project)
{
    mCanceled=false;
    QNetworkRequest request;
    QString serviceURL = mBaseURL+"/service/product/project/"+project->getCloudUUID();
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");


    request.setUrl(QUrl(serviceURL));
    setAuthorization(request, "service/product/project/"+project->getCloudUUID(), WEBSERVICE_REQUEST_TYPE_GET);

    QObject::connect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(onGetProductsListFinished(QNetworkReply*)));
    QNetworkReply *reply = mNetwork->get(request);
}

void WebServicesClient::getProject(Project *project)
{
    mCanceled=false;
    QNetworkRequest request;
    QString serviceURL = mBaseURL+"/service/project/"+project->getCloudUUID();
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");


    request.setUrl(QUrl(serviceURL));
    setAuthorization(request, "service/project/"+project->getCloudUUID(), WEBSERVICE_REQUEST_TYPE_GET);

    QObject::connect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(onGetProjectFinished(QNetworkReply*)));
    QNetworkReply *reply = mNetwork->get(request);

}

void WebServicesClient::setAuthorization(QNetworkRequest &request, QString service, int requestType, QString token, QString userID)
{
    // Date:
    QDateTime dt = QDateTime::currentDateTimeUtc();
    dt.setTimeSpec(Qt::UTC);
    QString dateString = dt.toString(Qt::ISODate);

    //nonce:
    qsrand((uint)dt.time().msec());
    QString nonce = "";
    for (int i=0; i<5; i++){
        int randomValue = 1111+rand()%(9999-1111);
        nonce = nonce +  QString::number(randomValue);
    }

    QString type;
    if (!requestType)
        type = "POST";
    else
        type = "GET";

    //Authorization:
    QString stringToHash = token + ":" + service + "," + type + "," + dateString + "," + nonce;
    QByteArray authorization = QString::fromStdString(sha256(stringToHash.toStdString())).toLatin1();
    authorization = userID.toLatin1() + ":" + QByteArray::fromHex(authorization).toBase64();

    request.setRawHeader("Authorization",authorization);
    request.setRawHeader("x-rest-date",dateString.toLatin1());
    request.setRawHeader("nonce",nonce.toLatin1());
}

void WebServicesClient::onLoginFinished(QNetworkReply *reply)
{
    QObject::disconnect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                        this, SLOT(onLoginFinished(QNetworkReply *)));

    if (reply->error() > 0) {
        mLoginData->logout();
        QString data = (QString)reply->readAll();
        int error = reply->error();
        QString erroString = reply->errorString();
        if(reply->error() == 99)
            emit loginFinished(mLoginData, tr("Network connection error"));
        else
            emit loginFinished(mLoginData, tr("User or passwod were incorrect"));
    }
    else {
        QString data = (QString)reply->readAll();
        QScriptEngine engine;
        QScriptValue result = engine.evaluate("("+data+")");
        QScriptValue entries = result.property("token");
        mLoginData->setToken(entries.toString());
        entries = result.property("userId");
        mLoginData->setUserId(entries.toString());
        mLoginData->setLogged(true);

        emit loginFinished(mLoginData, "");
    }
}

void WebServicesClient::onAddProjectFinished(QNetworkReply *reply)
{
    QObject::disconnect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(onAddProjectFinished(QNetworkReply *)));
    if (reply->error() > 0) {
        emit addProjectFinished(reply->errorString(), reply->errorString());
    }
    else{
        QString data = (QString)reply->readAll();
        QScriptEngine engine;
        QScriptValue result = engine.evaluate("("+data+")");
        QScriptValue projectUUID = result.property("uuid");
        emit addProjectFinished(projectUUID.toString(), "");
    }
}

void WebServicesClient::onAddImageFinished(QNetworkReply *reply){

    QObject::disconnect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(onAddImageFinished(QNetworkReply*)));

    if (reply->error() > 0) {
        emit addImagesFinished(reply->errorString(), reply->errorString());
    }
    else if (mCanceled){
        emit addImagesFinished("", "");
    }
    else{
        QString data = (QString)reply->readAll();
        QScriptEngine engine;
        QScriptValue result = engine.evaluate("("+data+")");
        QString imageUUID = result.property("uuid").toString();

        mUploadedImages++;
        QString uuid = mProject->getCloudUUID();
        if(mUploadedImages < mProject->getImages().count()){
            QNetworkRequest request;
            QString serviceURL = mBaseURL+"/service/imagen-project/add";
            request.setUrl(QUrl(serviceURL));

            QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

            QHttpPart textPart;
            textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"project\""));
            textPart.setBody(uuid.toLatin1());

            QHttpPart imagePart;
            imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/png"));
            QString fileName =  mProject->getImages().at(mUploadedImages)->getFileName();
            QString header = "form-data; name=\"file\"";
            header += "; filename=\"" + fileName + "\"";
            imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(header));
            QFile *file = new QFile(mProject->getImages().at(mUploadedImages)->getFullPath());
            file->open(QIODevice::ReadOnly);
            imagePart.setBodyDevice(file);
            file->setParent(multiPart); // we cannot delete the file now, so delete it with the multiPart

            multiPart->append(textPart);
            multiPart->append(imagePart);

            setAuthorization(request, "service/imagen-project/add", WEBSERVICE_REQUEST_TYPE_POST);
            QObject::connect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                             this, SLOT(onAddImageFinished(QNetworkReply*)));

            emit statusChanged(mUploadedImages, "Uploading " + mProject->getImages().at(mUploadedImages)->getFileName());

            QNetworkReply *reply = mNetwork->post(request,multiPart);
        }
        else
            emit addImagesFinished("1", "");
    }
}

void WebServicesClient::onAddProductsFinished(QNetworkReply * reply)
{
    QObject::disconnect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(onAddProductsFinished(QNetworkReply*)));

    if (reply->error() > 0) {
        emit addProductsFinished("", reply->errorString());
        return;
    }
    else{
        QString data = (QString)reply->readAll();
        QScriptEngine engine;
        QScriptValue result = engine.evaluate("("+data+")");
        QString productUUID = result.property("uuid").toString();
        if(mUploadedProducts == 0){
            mUploadedProducts++;
            QNetworkRequest request;
            QString serviceURL = mBaseURL+"/service/product/add";
            request.setUrl(QUrl(serviceURL));

            QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

            QHttpPart textPartProject;
            textPartProject.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"project\""));
            textPartProject.setBody(mProject->getCloudUUID().toLatin1());

            QHttpPart textPartType;
            textPartType.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"typeFile\""));
            textPartType.setBody(QByteArray("dense"));

            QHttpPart imagePart;
            imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/png"));
            QString fileName =  mProject->getDenseModelRelativePath();
            QString header = "form-data; name=\"file\"";
            header += "; filename=\"" + fileName + "\"";
            imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(header));
            QFile *file = new QFile(mProject->getDenseModelFullPath());
            file->open(QIODevice::ReadOnly);
            imagePart.setBodyDevice(file);
            file->setParent(multiPart); // we cannot delete the file now, so delete it with the multiPart

            multiPart->append(textPartProject);
            multiPart->append(textPartType);
            multiPart->append(imagePart);

            setAuthorization(request, "service/product/add", WEBSERVICE_REQUEST_TYPE_POST);
            QObject::connect(mNetwork, SIGNAL(finished(QNetworkReply *)),
                             this, SLOT(onAddProductsFinished(QNetworkReply*)));

            emit statusChanged(mUploadedImages, "Uploading " + mProject->getDenseModelRelativePath());

            QNetworkReply *reply = mNetwork->post(request,multiPart);
        }
        else
            emit addProductsFinished(productUUID, "");
    }
}

void WebServicesClient::onGetProductsListFinished(QNetworkReply *reply)
{
    disconnect(mNetwork, SIGNAL(finished(QNetworkReply *)),
               this, SLOT(onGetProductsListFinished(QNetworkReply*)));
    QList<Product*> productList;

    if (reply->error() > 0) {
        emit getProductListFinished(productList, reply->errorString());
        return;
    }
    QString data = (QString)reply->readAll();
    QScriptEngine engine;
    QScriptValue products = engine.evaluate("("+data+")").property("products");
    if(products.isArray())
    {
        QScriptValueIterator it(products);

        while (it.hasNext()) {
            it.next();
            if (it.name() == "length")
                continue;
            QScriptValue productValue = it.value();
            Product *product = new Product();
            product->setFileName(productValue.property("name").toString());
            product->setUUID(productValue.property("uuid").toString());
            product->setType(productValue.property("type").toString());
            productList.append(product);
        }
    }else{
        Product *product = new Product();
        product->setFileName(products.property("name").toString());
        product->setUUID(products.property("uuid").toString());
        product->setType(products.property("type").toString());
        productList.append(product);

    }

    emit getProductListFinished(productList, "");
}

void WebServicesClient::onGetProductFinished(QNetworkReply * reply)
{    
    disconnect(mNetwork, SIGNAL(finished(QNetworkReply *)),
               this, SLOT(onGetProductFinished(QNetworkReply*)));

    if (reply->error() > 0) {
        emit getProductFinished(NULL, NULL, reply->errorString());
        return;
    }else if (mCanceled){
        emit getProductsFinished("");
        return;
    }
    Product * product = mProducts.at(mDownloadedProducts);
    emit getProductFinished(product, (QIODevice *)reply, "");

    mDownloadedProducts++;
    if (mDownloadedProducts < mProducts.size()){
        product = mProducts.at(mDownloadedProducts);
        emit statusChanged(mDownloadedProducts, "Downloading "+ product->getFileName());
        getProduct(product);
    }
    else
        emit getProductsFinished("");
}

void WebServicesClient::onGetProjectFinished(QNetworkReply * reply)
{   
    disconnect(mNetwork, SIGNAL(finished(QNetworkReply *)),
               this, SLOT(onGetProjectFinished(QNetworkReply*)));

    Project project;
    if (reply->error() > 0) {
        QString error = reply->errorString();
        emit getProjectInformationFinished(project, reply->errorString());
        return;
    }

    QString data = (QString)reply->readAll();
    QScriptEngine engine;

    emit getProjectInformationFinished(project, "");
}

void WebServicesClient::onChangeStatusFinished(QNetworkReply *reply)
{
    if (reply->error() > 0) {
        emit changeStatusFinished(reply->errorString());
        return;
    }
    emit changeStatusFinished("");
}

void WebServicesClient::onCancelRequest()
{
    if(mCurrentReply)
        mCurrentReply->abort();
    mCanceled=true;
}


