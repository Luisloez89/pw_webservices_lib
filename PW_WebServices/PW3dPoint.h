#ifndef PW3DPOINT_H
#define PW3DPOINT_H
#include "pw_webservices_lib_global.h"
#include <QVector3D>
#include "PWPoint.h"


/**
 * @brief The PW3dPoint class
 */
class  PW_WEBSERVICES_LIBSHARED_EXPORT PW3dPoint : public PWPoint, public QVector3D
{

public:

    /**
         * @brief PW3dPoint
         */
        PW3dPoint();
        /**
     * @brief PW3dPoint
     * @param x
     * @param y
     * @param z
     * @param name
     * @param type
     */
    PW3dPoint(double x, double y, double z, QString name = "", PWPoint::PointType type = PWPoint::Control);
    /**
     * @brief The PointType enum
     */
    enum PointType {
        Control = 0x0000,
        Check = 0x0001
    };

    /**
     * @brief getCoordsCount Gets de number of components or dimensions of the point
     * @return
     */
    virtual int getCoordsCount();

    /**
     * @brief getCoord Gets the value of a component or coordinate.
     * @param index
     * @return
     */
    virtual double getCoord(int index);

    /**
     * @brief setCoord Sets the value of a component or coordinate.
     * @param index
     * @param value
     */
    void setCoord(int index, double value);


};
#endif // PW3DPOINT_H
