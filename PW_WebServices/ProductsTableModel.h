#ifndef PRODUCTSTABLEMODEL_H
#define PRODUCTSTABLEMODEL_H
#include "pw_webservices_lib_global.h"

#include <QAbstractTableModel>
#include "PW_WebServices/Product.h"

/**
 * @brief The ProductsTableModel class
 */
class PW_WEBSERVICES_LIBSHARED_EXPORT ProductsTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     * @param QObject
     */
    explicit ProductsTableModel(QObject *parent = 0);

    /**
     * @brief get number of elements
     * @param QModelIndex
     * @return number of elemets
     */
    int rowCount(const QModelIndex &parent) const;
    /**
     * @brief get number of atributes
     * @param QModelIndex
     * @return Number of atributes
     */
    int columnCount(const QModelIndex &parent) const;
    /**
     * @brief get atribute names
     * @param section
     * @param orientation
     * @param role
     * @return atribute names
     */
    QVariant headerData(int section, Qt::Orientation orientation,int role) const;
    /**
     * @brief data
     * @param index
     * @param role
     * @return data
     */
    QVariant data(const QModelIndex &index, int role) const;
    /**
     * @brief setData
     * @param index
     * @param value
     * @param role
     * @return success
     */
    bool setData (const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
    /**
     * @brief flags
     * @param index
     * @return
     */
    Qt::ItemFlags flags ( const QModelIndex & index ) const;
    /**
     * @brief set Products List
     * @param Products List
     */
    void setProductsList(QList<Product *> list);
    /**
     * @brief get Products List
     * @param boolean to get only ative item
     * @return Products List
     */
    QList<Product *> getProductsList(bool onlyAtive=false);



signals:

public slots:

private:
    /**
     * @brief Products List
     */
    QList<Product*> mProductsList;
    /**
     * @brief Check List
     */
    QList<bool>    mCheckList;

};

#endif // PRODUCTSTABLEMODEL_H
