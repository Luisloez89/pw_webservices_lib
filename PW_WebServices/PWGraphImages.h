#ifndef PWGRAPHIMAGES_H
#define PWGRAPHIMAGES_H
#include "pw_webservices_lib_global.h"

#include <QMap>
#include <QVector>

/**
 * @brief The PWGraphImages class
 */
class PW_WEBSERVICES_LIBSHARED_EXPORT PWGraphImages
{
public:
    /**
     * @brief Empty constructor
     */
    PWGraphImages();
    /**
     * @brief create Graph
     * @param images
     * @param image Pairs
     * @return
     */
    bool createGraph(QVector<QString> images,
                     QMap<QString, QVector<QString> > &imagePairs);
    /**
     * @brief check is Valid Graph
     * @return
     */
    bool isValid(){return mGraphIsConnected;}
    /**
     * @brief get Image Pairs
     * @param image Pairs
     */
    void getImagePairs(QMap<QString, QVector<QString> >& imagePairs);
    /**
     * @brief get Ordered Images
     * @param Graph number
     * @param ordered Images
     * @return
     */
    bool getOrderedImages(int nGraph,QVector<QString>& orderedImages);
    /**
     * @brief get Nth Ordered Images
     * @param number of images
     * @param ordered Images
     * @return
     */
    bool getNthOrderedImages(int nImages,QVector<QString>& orderedImages);
    /**
     * @brief get Invalid Matched Category Explanation
     * @param title
     * @param subgraphs
     */
    void getInvalidMatchedCategoryExplanation(QString& title,
                                              QStringList &subgraphs);
private:
    /**
     * @brief get Candidates
     * @param number of candidates
     * @param candidates list
     * @return
     */
    bool getCandidates(int nG,
                       QVector<QString>& candidates);
    /**
     * @brief get Candidate
     * @param number of candidate
     * @param candidates list
     * @param candidate name
     * @return
     */
    bool getCandidate(int nG,
                      QVector<QString> candidates,
                      QString& candidate);
    /**
     * @brief get Connections
     * @param initial Pos
     * @param connections
     * @param connected Images
     */
    void getConnections(int &initialPos,
                        QVector<QString>& connections,
                        QVector<QString>& connectedImages);
    /**
     * @brief Images list
     */
    QVector<QString> mImages;
    /**
     * @brief Image Pairs
     */
    QMap<QString, QVector<QString> > mImagePairs;
    /**
     * @brief Ordered Images Map
     */
    QVector<QVector<QString> > mOrderedImages;
    /**
     * @brief Unordered Images Map
     */
    QVector<QVector<QString> > mUnorderedImages;
    /**
     * @brief Level Images map
     */
    QVector<QMap<QString,int> > mLevelImages;
    /**
     * @brief Levels map
     */
    QVector< QVector<QVector<QString> > > mLevels;
    /**
     * @brief Graph Is Connected boolean
     */
    bool mGraphIsConnected;
};


#endif // PWGRAPHIMAGES_H
