#include "Product.h"

Product::Product(QObject *parent) :
    QObject(parent)
{
}
QString Product::getUUID()
{
    return mUUID;
}

void Product::setUUID(QString value)
{
    mUUID = value;
}
QString Product::getFileName()
{
    return mFileName;
}

void Product::setFileName(QString value)
{
    mFileName = value;
}
QString Product::getType()
{
    return mType;
}

void Product::setType(QString value)
{
    mType = value;
}
QString Product::getProjectUUID()
{
    return mProjectUUID;
}

void Product::setProjectUUID(QString value)
{
    mProjectUUID = value;
}




