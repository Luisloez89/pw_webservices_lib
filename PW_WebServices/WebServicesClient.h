#ifndef WEBSERVICESCLIENT_H
#define WEBSERVICESCLIENT_H

#define SESSION_STATUS_LOGGED               0
#define SESSION_STATUS_NOTLOGED             1
#define SESSION_STATUS_TIMEOUT              2

#define LOGIN_ERROR_USER                    0
#define LOGIN_ERROR_PASSWORD                1

#define WEBSERVICE_ERROR_UNKNOWN            0

#define WEBSERVICE_REQUEST_TYPE_POST        0
#define WEBSERVICE_REQUEST_TYPE_GET         1

#include "pw_webservices_lib_global.h"
#include <QString>
#include "PW_WebServices/LoggingData.h"
#include <QtNetwork/QNetworkReply>
#include "PW_WebServices/Project.h"
#include "PW_WebServices/Product.h"

/**
 * @brief The WebServicesClient class
 */
class PW_WEBSERVICES_LIBSHARED_EXPORT WebServicesClient : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief constructor with params
     * @param baseURL
     */
    WebServicesClient(QString baseURL);
    ~WebServicesClient();

    /**
     * @brief gets Base URL
     * @return Base URL
     */
    QString getBaseURL();
    /**
     * @brief set Base URL
     * @param base URL
     */
    void setBaseURL(QString &baseURL);
    /**
     * @brief check is Logged
     * @return is logged
     */
    bool isLogged();
    /**
     * @brief perform login
     * @param user
     * @param password
     */
    void login(QString user, QString password);
    /**
     * @brief get Session Status
     * @return Session Status
     */
    int getSessionStatus();
    /**
     * @brief add Project
     * @param project
     */
    void addProject(Project *project);
    /**
     * @brief add Images
     * @param project
     */
    void addImages(Project *project);
    /**
     * @brief add Products
     * @param project
     */
    void addProducts(Project *project);
    /**
     * @brief get Products
     * @param products
     */
    void getProducts(QList<Product *> products);
    /**
     * @brief get Product
     * @param project
     * @param type
     */
    void getProduct(Project *project, QString type);
    /**
     * @brief get Product
     * @param product
     */
    void getProduct(Product *product);
    /**
     * @brief get ProductsList
     * @param project
     */
    void getProductsList(Project *project);
    /**
     * @brief get Project
     * @param project
     */
    void getProject(Project *project);
    /**
     * @brief chage project Status
     * @param project
     * @param new status
     */
    void chageStatus(Project *project,QString status);

private:
    /**
     * @brief Base URL
     */
    QString mBaseURL;
    /**
     * @brief Login Data
     */
    LoginData * mLoginData;
    /**
     * @brief Network
     */
    QNetworkAccessManager * mNetwork;
    /**
     * @brief Current Reply
     */
    QNetworkReply *mCurrentReply;
    /**
     * @brief Uploaded Images
     */
    int mUploadedImages;
    /**
     * @brief Uploaded Products
     */
    int mUploadedProducts;
    /**
     * @brief Downloaded Products
     */
    int mDownloadedProducts;
    /**
     * @brief Project
     */
    Project *mProject;
    /**
     * @brief Products
     */
    QList< Product *> mProducts;
    /**
     * @brief Canceled
     */
    bool mCanceled;

    /**
     * @brief set request Authorization
     * @param request
     * @param service
     * @param requestType
     */
    void setAuthorization(QNetworkRequest &request, QString service, int requestType);
    /**
     * @brief set request Authorization
     * @param request
     * @param service
     * @param requestType
     * @param token
     * @param userID
     */
    void setAuthorization(QNetworkRequest &request, QString service, int requestType, QString token, QString userID);

private slots:
    /**
    * @brief slot on Login Finished
    * @param reply
    */
   void onLoginFinished(QNetworkReply* reply);
   /**
    * @brief slot on Add Project Finished
    * @param reply
    */
   void onAddProjectFinished(QNetworkReply *reply);
   /**
    * @brief slot on Add Image Finished
    * @param reply
    */
   void onAddImageFinished(QNetworkReply *reply);
   /**
    * @brief slot on Add Products Finished
    * @param reply
    */
   void onAddProductsFinished(QNetworkReply*reply);
   /**
    * @brief slot on Get Products List Finished
    * @param reply
    */
   void onGetProductsListFinished(QNetworkReply*reply);
   /**
    * @brief slot on Get Product Finished
    */
   void onGetProductFinished(QNetworkReply*);
   /**
    * @brief slot on Get Project Finished
    * @param reply
    */
   void onGetProjectFinished(QNetworkReply*reply);
   /**
    * @brief slot on Change Status Finished
    * @param reply
    */
   void onChangeStatusFinished(QNetworkReply *reply);
   /**
    * @brief slot on Cancel Request
    */
   void onCancelRequest();

signals:
   /**
    * @brief signal emited on login Finished
    * @param reply
    * @param errorString
    */
   void loginFinished(LoginData * reply, QString errorString);
   /**
    * @brief signal emited on add Project Finished
    * @param projectUUID
    * @param errorString
    */
   void addProjectFinished(QString projectUUID, QString errorString);
   /**
    * @brief signal emited on add Images Finished
    * @param imageUUID
    * @param errorString
    */
   void addImagesFinished(QString imageUUID, QString errorString);
   /**
    * @brief signal emited on add Products Finished
    * @param imageUUID
    * @param errorString
    */
   void addProductsFinished(QString imageUUID, QString errorString);
   /**
    * @brief signal emited on get Project Information Finished
    * @param project
    * @param errorString
    */
   void getProjectInformationFinished(Project project, QString errorString);
   /**
    * @brief signal emited on status Changed
    */
   void statusChanged(int,QString);
   /**
    * @brief signal emited on get Product List Finished
    * @param errorString
    */
   void getProductListFinished(QList<Product*>, QString errorString);
   /**
    * @brief signal emited on get Product Finished
    * @param product
    * @param fileDevice
    * @param errorString
    */
   void getProductFinished(Product* product, QIODevice *fileDevice, QString errorString);
   /**
    * @brief signal emited on get Products Finished
    * @param errorString
    */
   void getProductsFinished(QString errorString);
   /**
    * @brief signal emited on change Status Finished
    * @param errorString
    */
   void changeStatusFinished(QString errorString);
};

#endif // WEBSERVICESCLIENT_H
