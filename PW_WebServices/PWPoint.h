#ifndef PWPOINT_H
#define PWPOINT_H
#include "pw_webservices_lib_global.h"

#include <QObject>
#include <QList>


/**
 * @brief Base Class representing a generic point.
 */
class PW_WEBSERVICES_LIBSHARED_EXPORT PWPoint
{

public:

    /**
     * @brief The PointType enum
     */
    enum PointType {
        Control = 0x0000,
        Check = 0x0001
    };

    /**
     * @brief constructor with params
     * @param type
     * @param name
     */
    PWPoint(PointType type, QString name = "");
    /**
     * @brief Gets the type of point
     * @return type of point
     */
    virtual PointType getType();

    /**
     * @brief Gets de number of components or dimensions of the point
     * @return dimensions
     */
    virtual int getCoordsCount() = 0;

    /**
     * @brief Gets the value of a component or coordinate.
     * @param index
     * @return value
     */
    virtual double getCoord(int index) = 0;

    /**
     * @brief Gets a list of associated points (i.e.: Image points associated with a ground control point)
     * @return list of associated points
     */
    virtual QList<PWPoint *> getAssociatedPoints();

    /**
     * @brief Gets the name of the point.
     * @return name of the point
     */
    virtual QString getName();

    /**
     * @brief Sets the name of the point
     * @param name
     */
    virtual void setName(QString name);


    /**
     * @brief Sets the value of a component or coordinate.
     * @param index
     * @param Sets the value of a component
     */
    virtual void setCoord(int index, double value) = 0;

protected:
    /**
     * @brief List of associated points, i.e.: Image points associated with a ground control point
     */
    QList<PWPoint *> mAssociatedPoints;
    /**
     * @brief Type
     */
    PointType mType;
    /**
     * @brief Name
     */
    QString mName;
};

#endif // PWPOINT_H
