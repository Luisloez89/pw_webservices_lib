#include "LoggingData.h"

LoginData::LoginData():
    mUserName(""),
    mPassword(""),
    mToken(""),
    mNonce(""),
    mLogged(false)
{
}
QString LoginData::getUserName() const
{
    return mUserName;
}

void LoginData::setUserName(const QString &value)
{
    mUserName = value;
}
QString LoginData::getPassword() const
{
    return mPassword;
}

void LoginData::setPassword(const QString &value)
{
    mPassword = value;
}
QString LoginData::getToken() const
{
    return mToken;
}

void LoginData::setToken(const QString &value)
{
    mToken = value;
}
QString LoginData::getNonce() const
{
    return mNonce;
}

void LoginData::setNonce(const QString &value)
{
    mNonce = value;
}
bool LoginData::isLogged() const
{
    return mLogged;
}

void LoginData::setLogged(bool value)
{
    mLogged = value;
}

void LoginData::logout()
{
    mLogged = false;
    mUserName="";
    mPassword = "";
    mNonce="";
    mToken = "";
}
QString LoginData::getUserId() const
{
    return mUserId;
}

void LoginData::setUserId(const QString &value)
{
    mUserId = value;
}






