#include "ProductsTableModel.h"

ProductsTableModel::ProductsTableModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

int ProductsTableModel::rowCount(const QModelIndex &parent) const
{
    return mProductsList.count();
}

int ProductsTableModel::columnCount(const QModelIndex &parent) const
{
    return 2;
}

QVariant ProductsTableModel::data(const QModelIndex &index, int role) const
{
    if(index.isValid()){
        if (role == Qt::TextAlignmentRole)
            return int(Qt::AlignRight | Qt::AlignVCenter);
        else if (role == Qt::DisplayRole || role==Qt::EditRole) {
            if(index.column() == 0)
                return mProductsList.at(index.row())->getType();
            else if(index.column() == 1)
                return mProductsList.at(index.row())->getFileName();
            else if(index.column() == 2)
                return "";
        }
        else if(role == Qt::CheckStateRole){
            if(index.column() == 0)
                if (mCheckList.at(index.row()))
                    return Qt::Checked;
                else
                    return Qt::Unchecked;
            else return QVariant::Invalid;
        }
        else return QVariant::Invalid;
    }
}

QVariant ProductsTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || orientation != Qt::Horizontal)
        return QVariant();
    switch (section){
    case 0:
        return (tr("Product"));
        break;
    case 1:
        return (tr("Description"));
        break;
    }
}

bool ProductsTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    switch(index.column()){
    case 0:
        if (role == Qt::CheckStateRole)
        {
            if ((Qt::CheckState)value.toInt() == Qt::Checked)
            {
                mCheckList[index.row()] = true;
            }
            else
            {
                int checkedItems = 0;
                for (int i = 0; i < mCheckList.count(); i++)
                    if (mCheckList.at(i) == true)
                        checkedItems++;
                if(checkedItems > 1)
                    mCheckList[index.row()] = false;
            }
        }
        break;
    case 1:
        break;
    }
    return true;
}

Qt::ItemFlags ProductsTableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);
    if(index.column()==0)
        flags|=Qt::ItemIsUserCheckable;
    return flags;
}

void ProductsTableModel::setProductsList(QList<Product *> list)
{
    mProductsList = list;
    mCheckList.clear();
    for(int i=0; i< mProductsList.count(); i++)
        mCheckList.append(true);
}

QList<Product *> ProductsTableModel::getProductsList(bool onlyAtive)
{
    if(!onlyAtive)
        return mProductsList;
    else{
        QList<Product *> productsList;
        for(int i=0; i<mCheckList.count(); i++)
            if(mCheckList.at(i))
                productsList.append(mProductsList.at(i));
        return productsList;
    }
}
