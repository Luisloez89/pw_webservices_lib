#-------------------------------------------------
#
# Project created by QtCreator 2017-05-27T12:28:24
#
#-------------------------------------------------
debug_and_release {
    CONFIG -= debug_and_release
    CONFIG += debug_and_release
}
# ensure one "debug" or "release" in CONFIG so they can be used as
# conditionals instead of writing "CONFIG(debug, debug|release)"...
CONFIG(debug, debug|release) {
    CONFIG -= debug release
    CONFIG += debug
}
QT       += gui
QT += script
QT += network
CONFIG(debug, debug|release) {
    TARGET = PW_WebServices_libd
}
CONFIG(release, debug|release) {
    TARGET = PW_WebServices_lib
}

TEMPLATE = lib

DEFINES += PW_WEBSERVICES_LIB_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += PW_WebServices/PW_WebServices_lib.cpp \
    PW_WebServices/LoggingData.cpp \
    PW_WebServices/Product.cpp \
    PW_WebServices/ProductsTableModel.cpp \
    PW_WebServices/PW_WebServices_lib.cpp \
    PW_WebServices/sha256.cpp \
    PW_WebServices/WebServicesClient.cpp \
    PW_WebServices/Project.cpp \
    PW_WebServices/PW2dPoint.cpp \
    PW_WebServices/PW3dPoint.cpp \
    PW_WebServices/PWGraphImages.cpp \
    PW_WebServices/PWImage.cpp \
    PW_WebServices/PWPoint.cpp

HEADERS += PW_WebServices_lib.h\
        pw_webservices_lib_global.h \
    PW_WebServices/LoggingData.h \
    PW_WebServices/Product.h \
    PW_WebServices/ProductsTableModel.h \
    PW_WebServices/PW_WebServices_lib.h \
    PW_WebServices/pw_webservices_lib_global.h \
    PW_WebServices/sha256.h \
    PW_WebServices/WebServicesClient.h \
    PW_WebServices/Project.h \
    PW_WebServices/PW2dPoint.h \
    PW_WebServices/PW3dPoint.h \
    PW_WebServices/PWGraphImages.h \
    PW_WebServices/PWImage.h \
    PW_WebServices/PWPoint.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

INCLUDEPATH += $$PWD
