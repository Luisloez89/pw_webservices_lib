/****************************************************************************
** Meta object code from reading C++ file 'WebServicesClient.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../PW_WebServices/WebServicesClient.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'WebServicesClient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_WebServicesClient_t {
    QByteArrayData data[34];
    char stringdata0[539];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WebServicesClient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WebServicesClient_t qt_meta_stringdata_WebServicesClient = {
    {
QT_MOC_LITERAL(0, 0, 17), // "WebServicesClient"
QT_MOC_LITERAL(1, 18, 13), // "loginFinished"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 10), // "LoginData*"
QT_MOC_LITERAL(4, 44, 5), // "reply"
QT_MOC_LITERAL(5, 50, 11), // "errorString"
QT_MOC_LITERAL(6, 62, 18), // "addProjectFinished"
QT_MOC_LITERAL(7, 81, 11), // "projectUUID"
QT_MOC_LITERAL(8, 93, 17), // "addImagesFinished"
QT_MOC_LITERAL(9, 111, 9), // "imageUUID"
QT_MOC_LITERAL(10, 121, 19), // "addProductsFinished"
QT_MOC_LITERAL(11, 141, 29), // "getProjectInformationFinished"
QT_MOC_LITERAL(12, 171, 7), // "Project"
QT_MOC_LITERAL(13, 179, 7), // "project"
QT_MOC_LITERAL(14, 187, 13), // "statusChanged"
QT_MOC_LITERAL(15, 201, 22), // "getProductListFinished"
QT_MOC_LITERAL(16, 224, 15), // "QList<Product*>"
QT_MOC_LITERAL(17, 240, 18), // "getProductFinished"
QT_MOC_LITERAL(18, 259, 8), // "Product*"
QT_MOC_LITERAL(19, 268, 7), // "product"
QT_MOC_LITERAL(20, 276, 10), // "QIODevice*"
QT_MOC_LITERAL(21, 287, 10), // "fileDevice"
QT_MOC_LITERAL(22, 298, 19), // "getProductsFinished"
QT_MOC_LITERAL(23, 318, 20), // "changeStatusFinished"
QT_MOC_LITERAL(24, 339, 15), // "onLoginFinished"
QT_MOC_LITERAL(25, 355, 14), // "QNetworkReply*"
QT_MOC_LITERAL(26, 370, 20), // "onAddProjectFinished"
QT_MOC_LITERAL(27, 391, 18), // "onAddImageFinished"
QT_MOC_LITERAL(28, 410, 21), // "onAddProductsFinished"
QT_MOC_LITERAL(29, 432, 25), // "onGetProductsListFinished"
QT_MOC_LITERAL(30, 458, 20), // "onGetProductFinished"
QT_MOC_LITERAL(31, 479, 20), // "onGetProjectFinished"
QT_MOC_LITERAL(32, 500, 22), // "onChangeStatusFinished"
QT_MOC_LITERAL(33, 523, 15) // "onCancelRequest"

    },
    "WebServicesClient\0loginFinished\0\0"
    "LoginData*\0reply\0errorString\0"
    "addProjectFinished\0projectUUID\0"
    "addImagesFinished\0imageUUID\0"
    "addProductsFinished\0getProjectInformationFinished\0"
    "Project\0project\0statusChanged\0"
    "getProductListFinished\0QList<Product*>\0"
    "getProductFinished\0Product*\0product\0"
    "QIODevice*\0fileDevice\0getProductsFinished\0"
    "changeStatusFinished\0onLoginFinished\0"
    "QNetworkReply*\0onAddProjectFinished\0"
    "onAddImageFinished\0onAddProductsFinished\0"
    "onGetProductsListFinished\0"
    "onGetProductFinished\0onGetProjectFinished\0"
    "onChangeStatusFinished\0onCancelRequest"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WebServicesClient[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  109,    2, 0x06 /* Public */,
       6,    2,  114,    2, 0x06 /* Public */,
       8,    2,  119,    2, 0x06 /* Public */,
      10,    2,  124,    2, 0x06 /* Public */,
      11,    2,  129,    2, 0x06 /* Public */,
      14,    2,  134,    2, 0x06 /* Public */,
      15,    2,  139,    2, 0x06 /* Public */,
      17,    3,  144,    2, 0x06 /* Public */,
      22,    1,  151,    2, 0x06 /* Public */,
      23,    1,  154,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      24,    1,  157,    2, 0x08 /* Private */,
      26,    1,  160,    2, 0x08 /* Private */,
      27,    1,  163,    2, 0x08 /* Private */,
      28,    1,  166,    2, 0x08 /* Private */,
      29,    1,  169,    2, 0x08 /* Private */,
      30,    1,  172,    2, 0x08 /* Private */,
      31,    1,  175,    2, 0x08 /* Private */,
      32,    1,  178,    2, 0x08 /* Private */,
      33,    0,  181,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    4,    5,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    7,    5,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    9,    5,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    9,    5,
    QMetaType::Void, 0x80000000 | 12, QMetaType::QString,   13,    5,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,    2,    2,
    QMetaType::Void, 0x80000000 | 16, QMetaType::QString,    2,    5,
    QMetaType::Void, 0x80000000 | 18, 0x80000000 | 20, QMetaType::QString,   19,   21,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 25,    4,
    QMetaType::Void, 0x80000000 | 25,    4,
    QMetaType::Void, 0x80000000 | 25,    4,
    QMetaType::Void, 0x80000000 | 25,    4,
    QMetaType::Void, 0x80000000 | 25,    4,
    QMetaType::Void, 0x80000000 | 25,    2,
    QMetaType::Void, 0x80000000 | 25,    4,
    QMetaType::Void, 0x80000000 | 25,    4,
    QMetaType::Void,

       0        // eod
};

void WebServicesClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        WebServicesClient *_t = static_cast<WebServicesClient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->loginFinished((*reinterpret_cast< LoginData*(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->addProjectFinished((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 2: _t->addImagesFinished((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->addProductsFinished((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->getProjectInformationFinished((*reinterpret_cast< Project(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 5: _t->statusChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 6: _t->getProductListFinished((*reinterpret_cast< QList<Product*>(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 7: _t->getProductFinished((*reinterpret_cast< Product*(*)>(_a[1])),(*reinterpret_cast< QIODevice*(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 8: _t->getProductsFinished((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->changeStatusFinished((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->onLoginFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 11: _t->onAddProjectFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 12: _t->onAddImageFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 13: _t->onAddProductsFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 14: _t->onGetProductsListFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 15: _t->onGetProductFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 16: _t->onGetProjectFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 17: _t->onChangeStatusFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 18: _t->onCancelRequest(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<Product*> >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Product* >(); break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QIODevice* >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 15:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 16:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 17:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (WebServicesClient::*_t)(LoginData * , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WebServicesClient::loginFinished)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (WebServicesClient::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WebServicesClient::addProjectFinished)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (WebServicesClient::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WebServicesClient::addImagesFinished)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (WebServicesClient::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WebServicesClient::addProductsFinished)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (WebServicesClient::*_t)(Project , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WebServicesClient::getProjectInformationFinished)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (WebServicesClient::*_t)(int , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WebServicesClient::statusChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (WebServicesClient::*_t)(QList<Product*> , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WebServicesClient::getProductListFinished)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (WebServicesClient::*_t)(Product * , QIODevice * , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WebServicesClient::getProductFinished)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (WebServicesClient::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WebServicesClient::getProductsFinished)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (WebServicesClient::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&WebServicesClient::changeStatusFinished)) {
                *result = 9;
                return;
            }
        }
    }
}

const QMetaObject WebServicesClient::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_WebServicesClient.data,
      qt_meta_data_WebServicesClient,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *WebServicesClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WebServicesClient::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_WebServicesClient.stringdata0))
        return static_cast<void*>(const_cast< WebServicesClient*>(this));
    return QObject::qt_metacast(_clname);
}

int WebServicesClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void WebServicesClient::loginFinished(LoginData * _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void WebServicesClient::addProjectFinished(QString _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void WebServicesClient::addImagesFinished(QString _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void WebServicesClient::addProductsFinished(QString _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void WebServicesClient::getProjectInformationFinished(Project _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void WebServicesClient::statusChanged(int _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void WebServicesClient::getProductListFinished(QList<Product*> _t1, QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void WebServicesClient::getProductFinished(Product * _t1, QIODevice * _t2, QString _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void WebServicesClient::getProductsFinished(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void WebServicesClient::changeStatusFinished(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
