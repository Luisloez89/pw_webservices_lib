var class_p_w_image =
[
    [ "PWImage", "class_p_w_image.html#a3f22d8c0aa7ea700abf839edd0a0ce6d", null ],
    [ "~PWImage", "class_p_w_image.html#ab319b5f4384a711ca955202d9b442dc0", null ],
    [ "PWImage", "class_p_w_image.html#a3273eb13ce3ed5456d2f53a3557160ca", null ],
    [ "getControlPoints", "class_p_w_image.html#ac0c891b9aff3584ede4b1a2c80814a16", null ],
    [ "getFileName", "class_p_w_image.html#abf41ccbe34aa61ad4a5b8f5b6c0f3352", null ],
    [ "getFullPath", "class_p_w_image.html#ab84882d7214f3e3438ca786c8cddec5d", null ],
    [ "getID", "class_p_w_image.html#ac4d4f5b443a2fc2a2c3277768869c42b", null ],
    [ "getSize", "class_p_w_image.html#a3476cc379d7ecdf8d374f3fa86deac67", null ],
    [ "setBasePath", "class_p_w_image.html#ad182bf93b9e49cf13b264d8be139a9b1", null ],
    [ "setFullPath", "class_p_w_image.html#a186bdd8870fe64df65c1503a6cc0ecc1", null ],
    [ "setID", "class_p_w_image.html#a493161f73528c55fa9572106af87d19b", null ]
];