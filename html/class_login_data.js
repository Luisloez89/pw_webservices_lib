var class_login_data =
[
    [ "LoginData", "class_login_data.html#ad78189a6ab400117366dd1ee9ea72fee", null ],
    [ "getNonce", "class_login_data.html#a04d35e86132b4a9aac5a6b3a19e83f5d", null ],
    [ "getPassword", "class_login_data.html#a8a745ae4df5a0930ffe5fcab396492ae", null ],
    [ "getToken", "class_login_data.html#a4a1ccea6da4073ea00e9a4168ba0832b", null ],
    [ "getUserId", "class_login_data.html#a3a3f8924fb919a5972d1168aa5110aeb", null ],
    [ "getUserName", "class_login_data.html#aba79bcfa865058a752c63b14326364da", null ],
    [ "isLogged", "class_login_data.html#a0f0d9f810be05b4c2e799d124cd05fe8", null ],
    [ "logout", "class_login_data.html#a40161cd3e943bfe5b8f92459b2de6245", null ],
    [ "setLogged", "class_login_data.html#ae2a9f70d159ad24817e35a01b11691a3", null ],
    [ "setNonce", "class_login_data.html#a0705f7a5ceb4d3a0255023af96ee9846", null ],
    [ "setPassword", "class_login_data.html#a0258315a87ed6331745e85d1f7d3f26e", null ],
    [ "setToken", "class_login_data.html#a150712c4e8453ad4d17103db815ce6b4", null ],
    [ "setUserId", "class_login_data.html#a3ca4c17622c49649145c9762996c6be8", null ],
    [ "setUserName", "class_login_data.html#ac21f9ce4fa11777fd58c6e44f69d1764", null ]
];