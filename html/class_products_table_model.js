var class_products_table_model =
[
    [ "ProductsTableModel", "class_products_table_model.html#a5331c662344763754c3f41dec35962f7", null ],
    [ "columnCount", "class_products_table_model.html#aa2297830b09b27ff9eeaff9bc527c84d", null ],
    [ "data", "class_products_table_model.html#a16809ba7afc56e8c1366a6336152a13c", null ],
    [ "flags", "class_products_table_model.html#ac35d2e8cecc0b9f85ffb84a26d1b53d3", null ],
    [ "getProductsList", "class_products_table_model.html#aea9bf46e1665880ce034a8a7666c515b", null ],
    [ "headerData", "class_products_table_model.html#a9142219a1f2179474ca65b15bd6e6d5b", null ],
    [ "rowCount", "class_products_table_model.html#ac12d250720966ad4aaba80742b3a0116", null ],
    [ "setData", "class_products_table_model.html#a81f8078fac2e90a4374804279563cec5", null ],
    [ "setProductsList", "class_products_table_model.html#a8dd1dd9ef33e3faf811064bb9ba04860", null ]
];