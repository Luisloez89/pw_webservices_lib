var class_p_w2d_point =
[
    [ "PW2dPoint", "class_p_w2d_point.html#a23778abc1bdcc6a891af19e228eae38e", null ],
    [ "PW2dPoint", "class_p_w2d_point.html#af682d3f331df52083e611d89c2425879", null ],
    [ "getCoord", "class_p_w2d_point.html#a27d018a653c4c2b803735f3c050cf4a1", null ],
    [ "getCoordsCount", "class_p_w2d_point.html#ae4106f99a83d68b675af621008ba2572", null ],
    [ "getPoint", "class_p_w2d_point.html#a36cba1687ea4cd2380d62e991b67f4c2", null ],
    [ "setCoord", "class_p_w2d_point.html#a01828b7772e2cda182c951348c4869bb", null ]
];