var annotated_dup =
[
    [ "LoginData", "class_login_data.html", "class_login_data" ],
    [ "Product", "class_product.html", "class_product" ],
    [ "ProductsTableModel", "class_products_table_model.html", "class_products_table_model" ],
    [ "Project", "class_project.html", "class_project" ],
    [ "PW2dPoint", "class_p_w2d_point.html", "class_p_w2d_point" ],
    [ "PW3dPoint", "class_p_w3d_point.html", "class_p_w3d_point" ],
    [ "PW_WebServices_lib", "class_p_w___web_services__lib.html", "class_p_w___web_services__lib" ],
    [ "PWGraphImages", "class_p_w_graph_images.html", "class_p_w_graph_images" ],
    [ "PWImage", "class_p_w_image.html", "class_p_w_image" ],
    [ "PWPoint", "class_p_w_point.html", "class_p_w_point" ],
    [ "SHA256", "class_s_h_a256.html", "class_s_h_a256" ],
    [ "WebServicesClient", "class_web_services_client.html", "class_web_services_client" ]
];