var hierarchy =
[
    [ "LoginData", "class_login_data.html", null ],
    [ "Project", "class_project.html", null ],
    [ "PW_WebServices_lib", "class_p_w___web_services__lib.html", null ],
    [ "PWGraphImages", "class_p_w_graph_images.html", null ],
    [ "PWImage", "class_p_w_image.html", null ],
    [ "PWPoint", "class_p_w_point.html", [
      [ "PW2dPoint", "class_p_w2d_point.html", null ],
      [ "PW3dPoint", "class_p_w3d_point.html", null ]
    ] ],
    [ "QAbstractTableModel", null, [
      [ "ProductsTableModel", "class_products_table_model.html", null ]
    ] ],
    [ "QObject", null, [
      [ "Product", "class_product.html", null ],
      [ "WebServicesClient", "class_web_services_client.html", null ]
    ] ],
    [ "QPointF", null, [
      [ "PW2dPoint", "class_p_w2d_point.html", null ]
    ] ],
    [ "QVector3D", null, [
      [ "PW3dPoint", "class_p_w3d_point.html", null ]
    ] ],
    [ "SHA256", "class_s_h_a256.html", null ]
];