var class_product =
[
    [ "Product", "class_product.html#afe6aa3417fafe7e07bebe712a5611498", null ],
    [ "getFileName", "class_product.html#afeb91663c865e78e8001a8903e8e962e", null ],
    [ "getProjectUUID", "class_product.html#af1d79437538cbd810918a80e091828bd", null ],
    [ "getType", "class_product.html#a9ba9bc29e68344df2e8356e3df0e147d", null ],
    [ "getUUID", "class_product.html#a58331bb5b45683150f9399dca9296579", null ],
    [ "setFileName", "class_product.html#a2a225b23af1edf3a450083dcdaec3913", null ],
    [ "setProjectUUID", "class_product.html#a981e96b207ec9f8e40a44648c2b1adff", null ],
    [ "setType", "class_product.html#ad9ad82a2353c183b6da7a54d4eedf88b", null ],
    [ "setUUID", "class_product.html#abbfb5862c81491d38827d32e11879fca", null ]
];